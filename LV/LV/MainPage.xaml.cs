﻿using LV.Models;
using LV.ViewModels;
using System.Diagnostics;
using Xamarin.Forms;

namespace LV
{
    public partial class MainPage : ContentPage
    {
        MainPageViewModel MPVM { get; set; }
        public MainPage()
        {
            InitializeComponent();

            MPVM = new MainPageViewModel();

            this.BindingContext = MPVM;
            MyLV.ItemsSource = MPVM.MPM.ocMC;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            // doesn't matter what goes in here

        }
        private void OnAnswerPicker_Changed(object sender, ToggledEventArgs e)
        {
            Picker picker = sender as Picker;
            MyClass item = (MyClass)((Picker)sender).BindingContext;
            Debug.WriteLine("\n                             item " + item.ToString());
            Debug.WriteLine("\n MTVM.MTM.ocMC[item.reportID - 1] " + MPVM.MPM.ocMC[item.reportID - 1].ToString());
            

            MPVM.MPM.ocMC[item.reportID - 1].yesno = item.yesno;
            MPVM.MPM.ocMC[item.reportID - 1].Selected = item.Selected;
            Debug.WriteLine("\n MTVM.MTM.ocMC[item.reportID - 1] " + MPVM.MPM.ocMC[item.reportID - 1].ToString());

            Debug.WriteLine("\n ");

        }

    }
}
