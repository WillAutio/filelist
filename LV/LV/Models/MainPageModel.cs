﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace LV.Models
{
    public class MainPageModel : ObservableObject
    {
        public ObservableCollection<MyClass> ocMC { get; set; }

        public MainPageModel()
        {
            ocMC = new ObservableCollection<MyClass>();
            MyClass MC1 = new MyClass
            {
                myKey = 22,
                reportID = 1,
                yesno = null,
                explanation = "aaa",
                Selected = 0,
                question = "The first question."
            };
            ocMC.Add(MC1);
            MyClass MC2 = new MyClass
            {
                myKey = 23,
                reportID = 2,
                yesno = true,
                explanation = "bbb",
                Selected = 1,
                question = "The next question."
            };
            ocMC.Add(MC2);
            MyClass MC3 = new MyClass
            {
                myKey = 28,
                reportID = 3,
                yesno = null,
                explanation = "ccc",
                Selected = 0,
                question = "Another question."
            };
            ocMC.Add(MC3);
            MyClass MC4 = new MyClass
            {
                myKey = 22,
                reportID = 4,
                yesno = false,
                explanation = "ddd",
                Selected = 2,
                question = "Final question."
            };
            ocMC.Add(MC4);

        }
    }
}
