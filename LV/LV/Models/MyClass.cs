﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace LV.Models
{
    public class MyClass : ObservableObject
    {
        public int? myKey { get; set; }  // local primary key 

        public int reportID { get; set; } // 

        public bool? _yesno;
        public bool? yesno    // null => None; true => Yes; false => No;
        {
            get
            {
                return this._yesno;
            }
            set
            {
                if (_yesno != value)
                {
                    _yesno = value;
                   
                    OnPropertyChanged("yesno");
                }
            }

        }
        //public string explanation { get; set; }
        private string _explanation;
        public string explanation
        {
            get
            {
                return this._explanation;
            }
            set
            {
                if (_explanation != value)
                {
                    if (_explanation == null && value == "")
                    {  //  _explanation starts out as a null. The constructor sets it to "". 
                       //  That triggers a change that we do not care about.
                        _explanation = value;
                    }
                    else
                    {
                        _explanation = value;
                        toupload = 1; // on change to explanation mark this record for upload
                        OnPropertyChanged("explanation");
                    }
                }
            }
        }
        //public string prjnum { get; set; }

        public long projectID { get; set; }
        public int toupload { get; set; }    // 1 means the record needs to be uploaded

        public bool ShowToggle { get; set; }

        private bool _answerToggle;
        public bool AnswerToggle
        {
            get
            {
                return this._answerToggle;
            }
            set
            {
                if (_answerToggle != value)
                {
                    _answerToggle = value;
                    toupload = 1; // on change to AnswerToggle mark this record for upload
                    OnPropertyChanged("AnswerToggle");
                }
            }

        }

        public ObservableCollection<string> AnswerList { get; set; }

        public int _selected;
        public int Selected
        {
            get
            {
                return this._selected;
            }
            set
            {
                if (_selected != value)
                {
                    _selected = value;
                    toupload = 1; // on change to AnswerToggle mark this record for upload
                    if (_selected == 0)
                    {
                        yesno = null;
                    }
                    else if (_selected == 1)
                    {
                        yesno = true;
                    }
                    else if (_selected == 2)
                    {
                        yesno = false;
                    }

                    OnPropertyChanged("Selected");
                }
            }

        }

        public string question { get; set; }

        public MyClass()
        {
            AnswerList = new ObservableCollection<string>();
            AnswerList.Add("None");
            AnswerList.Add("Yes");
            AnswerList.Add("No");
        }

        public override string ToString()
        {
            string ans = 
                         " yesno  =" + yesno +
                         " Selected  =" + Selected +
                         " myKey  =" + myKey +
                         " reportID  =" + reportID +
                         " toupload  =" + toupload +
                         " projectID  =" + projectID +
                         " question  =" + question;
            return ans;
        }
    }
}
