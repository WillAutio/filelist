﻿using LV.Models;

namespace LV.ViewModels
{
    public class MainPageViewModel
    {
        public MainPageModel MPM { get; set; }

        public MainPageViewModel ()
        {
            MPM = new MainPageModel();
        }
    }
}
